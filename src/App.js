import React, { Component } from 'react';
import { Widget, addResponseMessage, setQuickButtons, toggleMsgLoader, addLinkSnippet, addUserMessage,fullScreenMode } from 'react-chat-widget';
import 'react-chat-widget/lib/styles.css';
import './App.css';
import Cookies from 'js-cookie';

import logo from './logo.svg';
import logo3 from './logo3.png';
import logo2 from './icon2.png';

var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

class App extends Component {
	state = { count: 0 };
	
	constructor(props) {
		super(props)
		
		this.count = this.count.bind(this)

		//console.log(parsed);
		//const Admin = 4000;
		//const Doctor = 3000;
		//const CaseManager = 2000;
		//const Paitent = 1000;
		//const None = 0;

		this.state = {
			counter: 0,
			idleMessage : '',
			hasPostidleMessage : false, 
			msgId : 0,
			time: {},
			seconds: 900,
			mode: '',// view/talk
			role: 0, //p: patient - 1000 c:case-manager - 2000
			booking_id: '',
			params: [],
			uid: '',
			items: [],
			isBlank: true,
			hasReceivedChatHistory: false
		}

		this.timer = 0;
		this.start_chat_time = true;
		this.last_msg_time = new Date();
		this.to_confirm_end_chat = false;
		//this.startTimer = this.startTimer.bind(this);
		//this.countDown = this.countDown.bind(this);
	}

	getSettingsFromCookies () {

		var booking_id = Cookies.get('booking_id');
		if(booking_id){
			this.setState({ booking_id: booking_id });
		}
		var mode = Cookies.get('mode');
		if(mode){
			this.setState({ mode: mode });
		}
		var role = Cookies.get('role');
		if(role){
			this.setState({ role: role });
		}
		var uid = Cookies.get('uid');
		if(uid){
			this.setState({ uid: uid });
		}

		console.log('mode:'+mode);
		console.log('role:'+role);
		console.log('booking_id:'+booking_id);
		console.log('uid:'+uid);
		this.getDataFetch();

	}

	async getDataFetch(){ // for view mode
		var json;
		if(this.state.booking_id && this.state.uid && this.state.mode && this.state.role){
			console.log(process.env.REACT_APP_RASAWEB_ENDPOINT+'/rasaweb/bot/get_msg.php?old_msg_id='+this.state.msgId+'&booking_id='+this.state.booking_id+'&uid='+this.state.uid+'&mode='+this.state.mode+'&role='+this.state.role);

			fetch(process.env.REACT_APP_RASAWEB_ENDPOINT+'/rasaweb/bot/get_msg.php?old_msg_id='+this.state.msgId+'&booking_id='+this.state.booking_id+'&uid='+this.state.uid+'&mode='+this.state.mode+'&role='+this.state.role)
			.then(json => json.json())
			.then(json =>
				this.addResponse(json)
			);
		}else{
			setTimeout(() => {
				this.getDataFetch();
			}, 3000);
			
		}
	}

	addResponse(json){
		var isBlank = true;
		console.log("incoming json: "+json);
		if (json && json.length > 0) {
			if(json.length > 1){
				json.sort((a, b) => a.id - b.id);
			}
			json.map((item) => {
				if(item.id != this.state.msgId){
					//console.log('inside');
					if(this.state.role==2000){ //case manager
						if(item.content){
							if(!item.content.includes('[') && !item.content.includes(']')){
								//itemContent = this.SplitUserMessage(item.content.toString());
								//addResponseMessage(itemContent);
								this.outPutMessage(item.content.toString(), true);
								isBlank = false
								console.log("hahaha");
								this.setState({ isBlank: false });
							}
						}
						if(item.response){
							//itemResponse = this.SplitUserMessage(item.response.toString());
							//addUserMessage(itemResponse);
							this.outPutMessage(item.response.toString(), false);
							isBlank = false;
							console.log("hahaha2");
							this.setState({ isBlank: false });
						}
					}else{

						//p
						if(!this.state.hasReceivedChatHistory){
								if(item.content){
									if(!item.content.includes('[') && !item.content.includes(']')){
										var itemContent = "";
										//itemContent = this.SplitUserMessage(item.content.toString());
										//addUserMessage(itemContent);
										this.outPutMessage(item.content.toString(), false);
										isBlank = false;
										console.log("hahaha3");
										this.setState({ isBlank: false });
									}
								}
								if(item.response){
									//itemResponse = this.outPutMessage(item.response.toString());
									//addResponseMessage(itemResponse);
									this.outPutMessage(item.response.toString(), true);
									isBlank = false;
									console.log("hahaha4");
									this.setState({ isBlank: false });
								}
						}else{
							//it's not a chat history
							if(!item.content.includes('[') && !item.content.includes(']')){
								if(item.content){
									//var itemContent = "";
									//itemContent = this.outPutMessage(item.content.toString());
									//addUserMessage(itemContent);
									this.outPutMessage(item.content.toString(), false);
									isBlank = false;
									console.log("hahaha3");
									this.setState({ isBlank: false });
								}
								if(item.response){
									var itemResponse = "";
									//itemResponse = this.outPutMessage(item.response.toString());
									//addResponseMessage(itemResponse);
									this.outPutMessage(item.response.toString(), true);
									isBlank = false;
									console.log("hahaha4");
									this.setState({ isBlank: false });
								}
								if(this.state.hasReceivedChatHistory){
									this.setState({ mode: 'talk' });
								}
						
							}
						}
						//when get a message , means case manager talking to you.
					}
					this.setState({ msgId: item.id });


					return (<div></div>);
				}
			})

			// if(this.state.role==1000 &&(this.state.role==1000 && this.state.hasReceivedChatHistory==false) && this.state.isBlank==true){
			if (this.state.role==1000 && this.state.hasReceivedChatHistory==false) {
				console.log('first msg');
				this.setState({ hasReceivedChatHistory: true });
				console.log("routex json: "+json+"json length: "+json.length);
					this.getBlankResponse();
			}
		}

		/*
		if(this.state.role==2000){ //case manager
			if(this.state.hasReceivedChatHistory){
				if(isBlank){
				}
				//if receive more then one chat , it's in the talk mode , where the case management took over the chat
				//this.setState({ mode: 'talk' });//????
			}
		}*/

		this.setState({ hasReceivedChatHistory: true });

		this.getDataFetch();
	}


	secondsToTime(secs){
		let hours = Math.floor(secs / (60 * 60));
	
		let divisor_for_minutes = secs % (60 * 60);
		let minutes = Math.floor(divisor_for_minutes / 60);
	
		let divisor_for_seconds = divisor_for_minutes % 60;
		let seconds = Math.ceil(divisor_for_seconds);
	
		let obj = {
		  "h": hours,
		  "m": minutes,
		  "s": seconds
		};
		return obj;
	  }
	  

    // Setup the `beforeunload` event listener
    setupBeforeUnloadListener = () => {
        window.addEventListener("beforeunload", (ev) => {
            ev.preventDefault();
            return this.unloadBrowser();
        });
	};

	getBlankResponse (idleMessage){
		var request = require('request');
		console.log("okok33333");
		var _this = this;
		request.post({
			headers: {'content-type' : 'application/x-www-form-urlencoded'},
			url:     process.env.REACT_APP_RASAWEB_ENDPOINT+'/rasaweb/bot/post_msg.php',
			body:    "message=[ask-question]&type=idle&booking_id="+this.state.booking_id+"&role="+this.state.role+'&uid='+this.state.uid+"&mode="+this.state.mode
		}, function(error, response, body){
			if(!error){
				if(body){

					var res_string = JSON.parse(body);
					console.log("meta_data",response);
					console.log(response.statusCode,res_string[0]['text']);
					if(idleMessage != null){
						if(res_string[0]['text'] !=""){
							//addResponseMessage(msg);
							var msg = _this.outPutMessage(res_string[0]['text'], true);
							return msg;
						}else{
							//this.getBlankResponse();
						}
					}else{
						var msg = _this.outPutMessage(res_string[0]['text'], true);
						return msg;
					}
				}
			}else{
				var msg = _this.outPutMessage("連線錯誤，訊息無法送出。", false);
				//addUserMessage();
				//return "連線錯誤，訊息無法送出。";
				console.log(error);
			}
		});
	}

	endIdle() {
		const msg = '請問仲有冇其他問題? 如冇請輸入《0》字或《再見》結束呢次對話';
		var current_time = new Date();
		var _this = this;
		// if get no msg more than one mintue
		if (this.last_msg_time && (current_time - this.last_msg_time)/1000 > 60) {
			this.last_msg_time = null;
			this.to_confirm_end_chat = true;

			var request = require('request');
			request.post({
				headers: {'content-type' : 'application/x-www-form-urlencoded'},
				url:     process.env.REACT_APP_RASAWEB_ENDPOINT+'/rasaweb/bot/post_msg.php',
				body:    "message=[ask-me-to-end]&type=idle&booking_id="+this.state.booking_id+"&role="+this.state.role+'&uid='+this.state.uid+"&mode="+this.state.mode
			}, function(error, response, body){
				if(!error){
					var res_string = JSON.parse(body);
					console.log("meta_data",response);
					console.log(response.statusCode,res_string[0]['text']);
						if(res_string[0]['text'] !=""){
							// addResponseMessage(res_string[0]['text']);
							var msg = _this.outPutMessage(res_string[0]['text'], true);
							return msg;
						}else{
							//this.getBlankResponse();
						}
				}else{
					addUserMessage("連線錯誤，訊息無法送出。");
					//return "連線錯誤，訊息無法送出。";
					console.log(error);
				}
			});
		}

		const pre_end_msgs = [
			"喺頭先個10分鐘，我哋已經傾過左你最關注嘅話題，係剩低個5分鐘你有咩想討論呢？",
			"我哋仲有5分鐘時間，可以繼續傾埋頭先個話題。",
			"仲有5分鐘時間，我哋可以傾一個你比較冇咁關注的話題。"
		];

		if(this.state.mode=="view" && this.state.role==1000){ //cm

			if (this.start_chat_time && this.state.seconds < 900 - 10*60 ) {
				var request = require('request');
				request.post({
					headers: {'content-type' : 'application/x-www-form-urlencoded'},
					url:     process.env.REACT_APP_RASAWEB_ENDPOINT+'/rasaweb/bot/post_msg.php',
					body:    "message=[pre-end]&type=idle&booking_id="+this.state.booking_id+"&role="+this.state.role+'&uid='+this.state.uid+"&mode="+this.state.mode
				}, function(error, response, body){
					if(!error){
						var res_string = JSON.parse(body);
						console.log("meta_data",response);
						console.log(response.statusCode,res_string[0]['text']);
							if(res_string[0]['text'] !=""){

									addResponseMessage(res_string[0]['text']);
								// this.outPutMessage(res_string[0]['text'], true);
								return res_string[0]['text'];
							}else{
								//this.getBlankResponse();
							}
					}else{
						addUserMessage("連線錯誤，訊息無法送出。");
						//return "連線錯誤，訊息無法送出。";
						console.log(error);
					}
				});
				// var pre_end_msg = pre_end_msgs[Math.floor(Math.random() * pre_end_msgs.length)];
				//addResponseMessage(pre_end_msg);
				// this.outPutMessage(pre_end_msg, true);

				this.start_chat_time = null;
			}
			
		}
	}

	componentWillUnmount() {
		this.timer = null; // here...

		Cookies.remove('mode');
		Cookies.remove('role');
		Cookies.remove('booking_id');
		Cookies.remove('uid');

		//console.log('mode:'+mode);
		//console.log('role:'+role);
		//console.log('booking_id:'+booking_id);
		//console.log('uid:'+uid);

	  }
	  
  componentDidMount() {
		this.deadline = 30;
		//this.connect();
		
		if(this.state.mode=="view" && this.state.role==2000){ //cm
		}

		if(this.state.mode=="talk"){
			//opening message.
			//this.setState({ hasPostidleMessage: true });
		}
		let timeLeftVar = this.secondsToTime(this.state.seconds);
		this.setState({ time: timeLeftVar });
		
		//addUserMessage(this.state.idleMessage);
		this.timerID = setInterval(
			() => this.endIdle(),
			1000		
		);
		this.x = setInterval(this.count, 1000);

		
		this.getSettingsFromCookies();

	}

	componentWillUnmount() {
		this.timer = null; // here...
		clearInterval(this.timerID);
	}

	count () {
		console.log("in..");
		// Remove one second, set state so a re-render happens.
		let seconds = this.state.seconds - 1;
		this.setState({
			time: this.secondsToTime(seconds),
			seconds: seconds,
		});
		
		// Check if we're at zero.
		if (seconds == 0 || this.state.counter == 900) {
			alert("本節對話已完。");
		}else{
			this.setState({ counter: this.state.counter + 1})
			if(this.state.counter>999 && this.state.hasPostidleMessage == false){
				//excced the time limit, post the idle message
				var msg = this.getBlankResponse(this);
				//log to server
				//handlePostMsg(msg, 'i') //i for idle
				this.setState({ counter: 0});
				this.setState({ idleMessage: msg });
				this.setState({ hasPostidleMessage: true });
			}
		}
	}

  handleNewUserMessage = (newMessage) => {
		//reset timer
		this.last_msg_time = new Date();
	    var getAnswerFromChatBot = false;
		if(this.state.role==1000 && this.state.mode=="view"){//p
			getAnswerFromChatBot = true;
		}
		if(getAnswerFromChatBot){
			toggleMsgLoader();
		}
			setTimeout(() => {
				if(getAnswerFromChatBot){
					toggleMsgLoader();
				}
						  var request = require('request');
						  var msg =  "message="+newMessage+"&role="+this.state.role+"&booking_id="+this.state.booking_id+'&uid='+this.state.uid+"&mode="+this.state.mode;
						  console.log(msg); 
						  request.post({
							headers: {'content-type' : 'application/x-www-form-urlencoded'},
							url:    process.env.REACT_APP_RASAWEB_ENDPOINT+'/rasaweb/bot/post_msg.php',
							body:    msg
						  }, function(error, response, body){
							  console.log('response'+response);
							  //console.log(error);
							
							  if(body){
												var res_string = JSON.parse(body);
												var text = res_string[0]['text'];
												console.log('text'+text);
									if(!error){
											if(getAnswerFromChatBot){
												console.log("meta_data",response);
												//this.outPutMessage(res_string[0]['text'], false);
												//this.outPutMessage(res_string[0]['text'], false);
												if(text){
													text = text.replace("？", "？\r");
													text = text.replace("。", "。\r");
													text = text.replace("！", "！\r");
													text = text.split("\r");
													text = text.forEach(function(_text) {
														if(_text != '' && _text != ' '){
															//if(role==1000){ //p
															//	addUserMessage(_text);
															//}else if(role==2000){ //cm
															//if(isReponse){
																addResponseMessage(_text);
															//}else{
															//	addUserMessage(text);
															//}
																//result = _text;
															//}
														}
													});

												
												}else{
													console.log(text);
													//result = text;
												}
												
											}
									}else{
										//addUserMessage("連線錯誤，訊息無法送出。");
										//this.outPutMessage("連線錯誤，訊息無法送出。", false);
										//this.outPutMessage("連線錯誤，訊息無法送出。", true);

										var text = "連線錯誤，訊息無法送出。";
										var result = '';
										if(text){
											text = text.replace("？", "？\r");
											text = text.replace("。", "。\r");
											text = text.replace("！", "！\r");
											text = text.split("\r");
											text = text.forEach(function(_text) {
												if(_text != '' && _text != ' '){
													//if(role==1000){ //p
													//	addUserMessage(_text);
													//}else if(role==2000){ //cm

													addUserMessage(text);
													
														//result = _text;
													//}
												}
											});

										
										}else{
											console.log(text);
											//result = text;
										}
										
										console.log(error);
									}
							  }
						  });
				//}
				  }, 3000);
				  this.setState({ hasPostidleMessage: false });
				  this.setState({ counter: 0 });
		//}
  }
  

  outPutMessage(text, isReponse) {
	var result = '';
	if(text){
		text = text.replace("？", "？\r");
		text = text.replace("。", "。\r");
		text = text.replace("！", "！\r");
		text = text.split("\r");
		text = text.forEach(function(_text) {
			if(_text != '' && _text != ' '){
				//if(role==1000){ //p
				//	addUserMessage(_text);
				//}else if(role==2000){ //cm
				if(isReponse){
					addResponseMessage(_text);
				}else{
					addUserMessage(_text);
				}
					//result = _text;
				//}
			}
		});

	
	}else{
		console.log(text);
		//result = text;
	}


	//return text;	

  }

  render() {
		const { days, seconds, hours, minutes, time_up } = this.state
    return (
		<div>
			<div class="text-center">
					  <img src={process.env.REACT_APP_RASAWEB_ENDPOINT +"/rasaweb/img/logo.jpeg"} height="211" width="300" / >
			</div>
			<div class="marginleft">時間剩餘 : {this.state.time.m} 分 {this.state.time.s} 秒 </div>
      <Widget
        title="智能姑娘"
        subtitle=""
        senderPlaceHolder="在此輸入 ..."
        handleNewUserMessage={this.handleNewUserMessage}
        handleQuickButtonClicked={this.handleQuickButtonClicked}
        badge={0}
        profileAvatar={logo2}
        titleAvatar={logo3}
        fullScreenMode={0}
        autofocus={0}
		launcher={handleToggle => (
		  <div id="div-toogle" onClick={handleToggle}><img id='launcher-icon' src={logo2} ></img></div>
		)}
      />
	  </div>
    );
  }
}

export default App;
